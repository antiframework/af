```java

package com.example.demo;

import com.af.commons.BeanProxy;
import com.af.jpa.SQL;
import com.af.jpa.dialects.HSQLDialect;
import com.af.reactive.trx.ReactiveSQL;
import com.af.reactive.trx.ReactiveTransactionWrapper;
import com.af.reactive.utils.AsyncExecutor2;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.scheduler.Scheduler;

import javax.sql.DataSource;

@Configuration
public class DbConfiguration {

	@Bean
	@ConfigurationProperties("spring.datasource")
	public HikariConfig hikariConfig() {
		return new HikariConfig();
	}

	@Bean
	public DataSource dataSource(HikariConfig hikariConfig) {
		return new HikariDataSource(hikariConfig);
	}

	@Bean
	public Scheduler scheduler() {

		return new AsyncExecutor2("ReactiveSQL", 1, 1000, 10000);
	}

	@Bean
	public ReactiveTransactionInstrumenter reactiveTransactionInstrumenter(Scheduler scheduler, DataSource dataSource) {

		return new ReactiveTransactionInstrumenter(scheduler, dataSource);
	}

	@Bean
	public BeanProxy beanProxy(ReactiveTransactionInstrumenter reactiveTransactionInstrumenter) {

		return new BeanProxy(/*new SubscriberContextMarker(), */reactiveTransactionInstrumenter);
	}

	@Bean
	public ReactiveSQL reactiveSQL(Scheduler scheduler) {

		return new ReactiveSQL(scheduler, new SQL(new HSQLDialect()));
	}
}

```



```java
package com.example.demo;

import com.af.jpa.queries.ReflectionEntityConstructor;
import com.af.reactive.trx.ReactiveSQL;
import com.af.reactive.utils.PressureControl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Collections;

@Slf4j
@Controller
@RequestMapping("a")
public class DemoController {

	@Autowired
	private ReactiveSQL reactiveSQL;

	private ReflectionEntityConstructor<UserEntity> userEntityConstructor;

	@PostConstruct
	public void init() {

		userEntityConstructor = new ReflectionEntityConstructor<>(UserEntity.class)
				.addSub("address", new ReflectionEntityConstructor<>(AddressEntity.class));
	}

	@Transactional
	@RequestMapping(path = "b", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Flux<UserEntity> b() {

		return Flux.fromArray(new String[]{"Alice", "Bob", "Pooh the bear"})
				.map(name -> {

					UserEntity userEntity = new UserEntity();
					userEntity.setName(name);

					return userEntity;
				})
				.as(f -> new PressureControl<>(f, 1))
				.flatMap(reactiveSQL::save)
				.flatMap(u -> {

					AddressEntity a = new AddressEntity();
					a.setUserId(u.getId());
					a.setDescription("Address of " + u.getName());

					return reactiveSQL.save(a)
							.doOnNext(u::setAddress)
							.map(aa -> u);
				}).thenMany(reactiveSQL.fetchAll(userEntityConstructor, "select u.*, a.description, a.user_id from users u left join addresses a on a.user_id = u.id", Collections.emptyList()));
	}

}

```