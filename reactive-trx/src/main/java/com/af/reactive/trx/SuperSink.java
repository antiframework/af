/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import reactor.core.Disposable;
import reactor.util.context.Context;

import java.util.function.LongConsumer;

public interface SuperSink<T> {

	Context currentContext();
	void onRequest(LongConsumer consumer);
	void onCancel(Disposable d);
	void error(Throwable e);
	void next(T t);
	void complete();
}
