/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.LinkedList;

@Getter
@Setter
public class ReactiveTrxContext {

	private volatile Connection connection;
	private volatile String datasourceName;
}
