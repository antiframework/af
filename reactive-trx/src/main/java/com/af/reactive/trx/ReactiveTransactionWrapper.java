/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import com.af.commons.SuperCallable;
import com.af.jpa.queries.Datasource;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.BeanCreationException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.lang.reflect.Method;

@SuppressWarnings("unused")
@Slf4j
public class ReactiveTransactionWrapper {

	private final Scheduler scheduler;
	private final DataSource dataSource;
	private final String datasourceName;

	public ReactiveTransactionWrapper(Scheduler scheduler, DataSource dataSource, String datasourceName) {

		this.scheduler = scheduler;
		this.dataSource = dataSource;
		this.datasourceName = datasourceName;
	}

	public Object execute(Method originalMethod, SuperCallable caller) throws Throwable {

		if (isEligible(originalMethod)) {

			boolean requireNew = isRequireNew(originalMethod);

			log.debug("Proxying bean for class " + originalMethod.getDeclaringClass().getName() + ", method: " + originalMethod.getName());

			@SuppressWarnings("unchecked")
			Publisher<Object> upstreamPublisher = (Publisher<Object>) caller.call();

			if (Mono.class.isAssignableFrom(originalMethod.getReturnType())) {

				return Mono.just(Boolean.TRUE)
						.publishOn(scheduler)
						.flatMap(v -> Mono.create(monoSink ->
								TransactionalPublisher.create(new MonoSuperSink<>(monoSink), dataSource, upstreamPublisher, !requireNew, datasourceName == null ? "@@@@@@@" : datasourceName)));
			}

			if (Flux.class.isAssignableFrom(originalMethod.getReturnType())) {

				return Mono.just(Boolean.TRUE)
						.publishOn(scheduler)
						.flatMapMany(v -> Flux.create(fluxSink ->
								TransactionalPublisher.create(new FluxSuperSink<>(fluxSink), dataSource, upstreamPublisher, !requireNew, datasourceName == null ? "@@@@@@@" : datasourceName)));
			}

			throw new Exception("Unsupported return type for transaction instrumentation: " + originalMethod.getReturnType());
		}

		return caller.call();
	}

	private boolean isRequireNew(Method method) {

		switch (method.getAnnotation(Transactional.class).value()) {

			case REQUIRED:
				return false;
			case REQUIRES_NEW:
				return true;
			default:
				throw new BeanCreationException("Unable to create bean " + method.getDeclaringClass().getName() + ", transaction isolation level is not supported: " + method.getAnnotation(Transactional.class).value());
		}
	}


	private boolean isEligible(Method method) {

		if (method.getAnnotation(Transactional.class) == null || !Publisher.class.isAssignableFrom(method.getReturnType()))
			return false;

		Datasource datasourceAnn = method.getAnnotation(Datasource.class);
		if (datasourceAnn == null && datasourceName == null)
			return true;

		return datasourceAnn != null && datasourceName != null && datasourceName.equals(datasourceAnn.value());
	}
}
