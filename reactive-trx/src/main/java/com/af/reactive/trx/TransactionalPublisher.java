/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import com.af.jpa.AntiframeworkJpaException;
import com.af.reactive.utils.SubscriberContextMarker;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.util.context.Context;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class TransactionalPublisher {

	private static final String AF_TRX_CONTEXT = "AF_TRX_CONTEXT";

	public static Connection connection(Context context) throws AntiframeworkJpaException {

		ReactiveTrxContext trxContext = context.getOrDefault(AF_TRX_CONTEXT, null);
		if (trxContext == null)
			throw new AntiframeworkJpaException("Not in a transactional context");

		if (trxContext.getConnection() == null)
			throw new AntiframeworkJpaException("No connection in trx context");

		Connection conn = trxContext.getConnection();
		log.debug("Supply connection, datasourceName: " + trxContext.getDatasourceName());

		return conn;
	}

	public static <T> void create(SuperSink<T> superSink, DataSource dataSource, Publisher<T> upstreamPublisher, boolean join, String datasourceName) {

		try {

			AtomicBoolean connectionCreator = new AtomicBoolean(false);

			AtomicReference<Context> context = new AtomicReference<>(superSink.currentContext());
			@SuppressWarnings("deprecation")
			String contextMarker = SubscriberContextMarker.marker(context.get());

			if (!context.get().hasKey(AF_TRX_CONTEXT)) {

				log.debug(contextMarker + ": Reactive context did not have transaction context, adding...");
				context.set(context.get().put(AF_TRX_CONTEXT, new ReactiveTrxContext()));
			}

			ReactiveTrxContext trxContext = context.get().get(AF_TRX_CONTEXT);

			AtomicReference<Connection> connAtomic = new AtomicReference<>(), outerConnAtomic = new AtomicReference<>(trxContext.getConnection());
			AtomicReference<String> outerDatasourceNameAtomic = new AtomicReference<>(trxContext.getDatasourceName());
			AtomicReference<Subscription> upstreamSubscription = new AtomicReference<>();

			AtomicLong requested = new AtomicLong(0);

			superSink.onRequest(r -> {

				if (upstreamSubscription.get() != null)
					upstreamSubscription.get().request(r);
				else
					requested.addAndGet(r);
			});

			superSink.onCancel(() -> {

				try {

					log.debug(contextMarker + ": Canceled");

					if (upstreamSubscription.get() != null)
						upstreamSubscription.get().cancel();

					if (!connectionCreator.get()) {
						log.debug(contextMarker + ": The connection wasn't created by this transaction. Doing nothing");
					} else {

						Connection conn = connAtomic.get();

						if (conn != null) {
							conn.rollback();
							log.debug(contextMarker + ": Rolled back");

							conn.setAutoCommit(true);
							conn.close();
							log.debug(contextMarker + ": Connection closed");
						} else
							log.debug(contextMarker + ": The connection wasn't created yet. Doing nothing");

						trxContext.setConnection(outerConnAtomic.get());
						trxContext.setDatasourceName(outerDatasourceNameAtomic.get());
					}
				} catch (Exception ex) {

					superSink.error(ex);
				}

			});

			if (!join || trxContext.getConnection() == null || !trxContext.getDatasourceName().equalsIgnoreCase(datasourceName)) {

				log.debug(contextMarker + ": Adding new connection to the transaction context, datasourceName: " + datasourceName + " join: " + join + ", current connections: " + (trxContext.getConnection() != null));

				connectionCreator.set(true);
				Connection conn = dataSource.getConnection();
				conn.setAutoCommit(false);

				log.debug("Adding connection,  datasourceName: " + datasourceName + ", outer: " + outerConnAtomic.get() + ", current: " + conn);
				trxContext.setConnection(conn);
				trxContext.setDatasourceName(datasourceName);
				connAtomic.set(conn);
				log.debug(contextMarker + ": New connection added");

			} else {

				log.debug(contextMarker + ": NOT - Adding new connection to the transaction context, datasourceName: " + datasourceName + ", join: " + join + ", current connections: " + (trxContext.getConnection() != null));
				connectionCreator.set(false);
				connAtomic.set(trxContext.getConnection());
			}


			upstreamPublisher.subscribe(new CoreSubscriber<T>() {

				@Override
				public void onSubscribe(Subscription subscription) {

					if (requested.get() > 0) {
						subscription.request(requested.get());
						requested.set(0);
					}
				}

				@Override
				public void onNext(T t) {

					log.debug(contextMarker + ": new row from DB, datasourceName: " + trxContext.getDatasourceName() + "");
					superSink.next(t);
				}

				@Override
				public void onError(Throwable e) {
					try {

						log.debug(contextMarker + ": Rolling back");

						if (!connectionCreator.get()) {
							log.debug(contextMarker + ": The connection wasn't created by this transaction, datasourceName: " + trxContext.getDatasourceName() + ". Doing nothing");
						} else {

							Connection conn = connAtomic.get();

							if (conn != null) {
								conn.rollback();
								log.debug(contextMarker + ": Rolled back");

								conn.setAutoCommit(true);
								conn.close();
								log.debug(contextMarker + ": Connection closed, , datasourceName: " + trxContext.getDatasourceName() );
							} else
								log.debug(contextMarker + ": The connection wasn't created yet. Doing nothing");

							trxContext.setConnection(outerConnAtomic.get());
							trxContext.setDatasourceName(outerDatasourceNameAtomic.get());
						}

						superSink.error(e);
					} catch (Exception ex) {

						superSink.error(ex);
					}
				}

				@Override
				public void onComplete() {

					try {

						log.debug(contextMarker + ": The transaction completed " + trxContext.getDatasourceName());

						if (!connectionCreator.get()) {
							log.debug(contextMarker + ": The connection wasn't created by this transaction, datasourceName: " + trxContext.getDatasourceName() + ". Doing nothing");
						} else {

							Connection conn = connAtomic.get();

							if (conn != null) {

								conn.commit();
								log.debug(contextMarker + ": Connection committed " + trxContext.getDatasourceName());

								conn.setAutoCommit(true);

								conn.close();
								log.debug(contextMarker + ": Connection closed, datasourceName: " + trxContext.getDatasourceName() + "");
							} else
								log.debug(contextMarker + ": The connection wasn't created yet. Doing nothing");

							log.debug("Restoring outer connection: outer: " + outerDatasourceNameAtomic.get() + ", current: " + trxContext.getDatasourceName());
							trxContext.setConnection(outerConnAtomic.get());
							trxContext.setDatasourceName(outerDatasourceNameAtomic.get());
						}

						superSink.complete();
					} catch (Exception ex) {

						superSink.error(ex);
					}
				}

				@Override
				public Context currentContext() {
					return context.get();
				}
			});

		} catch (SQLException ex) {

			superSink.error(ex);
		}
	}
}
