/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import lombok.AllArgsConstructor;
import reactor.core.Disposable;
import reactor.core.publisher.MonoSink;
import reactor.util.context.Context;

import java.util.function.LongConsumer;

@AllArgsConstructor
public class MonoSuperSink<T> implements SuperSink<T> {

	private MonoSink<T> real;

	@Override
	public Context currentContext() {
		return real.currentContext();
	}

	@Override
	public void onRequest(LongConsumer consumer) {
		real.onRequest(consumer);
	}

	@Override
	public void onCancel(Disposable d) {
		real.onCancel(d);
	}

	@Override
	public void error(Throwable e) {
		real.error(e);
	}

	@Override
	public void next(T t) {
		real.success(t);
	}

	@Override
	public void complete() {
		real.success();
	}
}
