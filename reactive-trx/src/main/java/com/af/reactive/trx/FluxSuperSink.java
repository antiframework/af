/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import lombok.AllArgsConstructor;
import reactor.core.Disposable;
import reactor.core.publisher.FluxSink;
import reactor.util.context.Context;

import java.util.function.LongConsumer;

@AllArgsConstructor
public class FluxSuperSink<T> implements SuperSink<T> {

	private FluxSink<T> real;

	@Override
	public Context currentContext() {
		return real.currentContext();
	}

	@Override
	public void onRequest(LongConsumer consumer) {
		real.onRequest(consumer);
	}

	@Override
	public void onCancel(Disposable d) {
		real.onCancel(d);
	}

	@Override
	public void error(Throwable e) {
		real.error(e);
	}

	@Override
	public void next(T t) {
		real.next(t);
	}

	@Override
	public void complete() {
		real.complete();
	}
}
