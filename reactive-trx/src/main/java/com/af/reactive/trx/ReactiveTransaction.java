/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import reactor.core.publisher.Mono;

public interface ReactiveTransaction {

	Mono<Void> commit();
	Mono<Void> rollback();
}
