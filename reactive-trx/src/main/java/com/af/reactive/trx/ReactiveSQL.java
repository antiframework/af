/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.trx;

import com.af.commons.CloseableIterator;
import com.af.jpa.SQL;
import com.af.jpa.queries.EntityConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("unused")
@Slf4j
public class ReactiveSQL {

	private final Scheduler scheduler;
	private final SQL sql;

	public ReactiveSQL(Scheduler scheduler, SQL sql) {

		this.scheduler = scheduler;
		this.sql = sql;
	}

	public <E> Mono<E> save(E entity) {

		return Mono.just(entity)
				.publishOn(scheduler)
				.flatMap(e -> Mono.create(monoSink -> {

					try {

						Connection conn = TransactionalPublisher.connection(monoSink.currentContext());
						sql.save(conn, entity);

						monoSink.success(entity);
					} catch (Exception ex) {

						monoSink.error(ex);
					}
				}));
	}

	public <T> Mono<T> fetchById(EntityConstructor<T> entityConstructor, Object id) {

		return Mono.just(Boolean.TRUE)
				.publishOn(scheduler)
				.flatMap(v -> Mono.create(monoSink -> {

					try {

						Connection conn = TransactionalPublisher.connection(monoSink.currentContext());
						Optional<T> t = sql.fetchById(conn, entityConstructor, id);

						if (t.isPresent())
							monoSink.success(t.get());
						else
							monoSink.success();
					} catch (Exception ex) {

						monoSink.error(ex);
					}
				}));
	}

	public <T> Flux<T> fetchAll(EntityConstructor<T> entityConstructor, String query, List<?> params) {

		return Mono.just(Boolean.TRUE)
				.publishOn(scheduler)
				.flatMapMany(v -> Flux.create(fluxSink -> {

					AtomicReference<CloseableIterator<T>> it = new AtomicReference<>();
					AtomicBoolean canceled = new AtomicBoolean(false);
					fluxSink.onCancel(() -> {

						try {

							log.debug("Canceled, query: " + query);
							canceled.set(true);

							if (it.get() != null)
								it.get().close();
						} catch (Exception ex) {

							log.warn("Error closing result set", ex);
						}
					});

					fluxSink.onRequest(items -> {

						try {

							log.debug("Requested, items: " + items + ", query: " + query);

							if (it.get() == null) {

								Connection conn = TransactionalPublisher.connection(fluxSink.currentContext());
								it.set(sql.fetchAll(conn, entityConstructor, query, params));
							}

							if (canceled.get())
								return;

							while (items -- > 0) {

								if (it.get().hasNext())
									fluxSink.next(it.get().next());
								else {

									fluxSink.complete();
									break;
								}
							}

						} catch (Exception ex) {

							fluxSink.error(ex);
						}
					});
				}));
	}

	public Mono<Integer> update(String query, List<?> params) {

		return Mono.just(Boolean.TRUE)
				.publishOn(scheduler)
				.flatMap(v -> Mono.create(monoSink -> {

					try {

						Connection conn = TransactionalPublisher.connection(monoSink.currentContext());
						int count = sql.update(conn, query, params);

						monoSink.success(count);
					} catch (Exception ex) {

						monoSink.error(ex);
					}
				}));
	}

	public <T> Mono<T> fetchOne(EntityConstructor<T> entityConstructor, String query, List<?> params) {

		return Mono.just(Boolean.TRUE)
				.publishOn(scheduler)
				.flatMap(v -> Mono.create(monoSink -> {

					try {

						Connection conn = TransactionalPublisher.connection(monoSink.currentContext());
						Optional<T> t = sql.fetchOne(conn, entityConstructor, query, params);

						if (t.isPresent())
							monoSink.success(t.get());
						else
							monoSink.success();
					} catch (Exception ex) {

						monoSink.error(ex);
					}
				}));
	}

	public Mono<Integer> delete(Class<?> clazz, Object id) {

		return Mono.just(Boolean.TRUE)
				.publishOn(scheduler)
				.flatMap(v -> Mono.create(monoSink -> {

					try {

						Connection conn = TransactionalPublisher.connection(monoSink.currentContext());
						int count = sql.delete(conn, clazz, id);

						monoSink.success(count);
					} catch (Exception ex) {

						monoSink.error(ex);
					}
				}));
	}
}
