/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Stream;

@SuppressWarnings("PublisherImplementation")
@Slf4j
public class AsyncExcecuter implements Executor {

	private int numOfThreads;
	private int capacity;
	private ArrayBlockingQueue<Runnable> queue;

	public AsyncExcecuter(int numOfThreads, int capacity) {

		this.numOfThreads = numOfThreads;
		this.capacity = capacity;

		createQueue();
	}

	@Override
	public void execute(Runnable command) {

		queue.add(command);
	}

	public <V> Mono<V> execute(Routine<V> c) {

		return Mono.from(_execute((subscriber, cancelled, logPrefix) -> {

			V result = c.execute(subscriber instanceof CoreSubscriber ? ((CoreSubscriber<?>) subscriber).currentContext() : null);

			if (!cancelled.get()) {

				if (result != null) {

					log.debug(logPrefix + ", Going to fire next: " + result);
					subscriber.onNext(result);
				} else
					log.debug(logPrefix + ", No value to fire as next");

				log.debug(logPrefix + ", Going to fire complete");
				subscriber.onComplete();
			} else
				log.debug(logPrefix + ", Have result but canceled: ");
		}));
	}

	public <V> Flux<V> executeMany(Routine2<V> c) {

		return Flux.from(_execute((subscriber, cancelled, logPrefix) -> {

			c.execute(subscriber instanceof CoreSubscriber ? ((CoreSubscriber<?>) subscriber).currentContext() : null, v -> {

				if (cancelled.get()) {

					log.debug(logPrefix + ", Have result but canceled: " + v);
					return false;
				}

				log.debug(logPrefix + ", Going to fire next: " + v);
				subscriber.onNext(v);

				return true;
			});

			if (!cancelled.get()) {

				log.debug(logPrefix + ", Going to fire complete");
				subscriber.onComplete();
			} else
				log.debug(logPrefix + ", Going to fire complete but cancelled");
		}));
	}

	private interface Core<V> {

		void execute(Subscriber<? super V> subscriber, AtomicBoolean canceled, String logPrefix) throws Exception;
	}

	private <V> Publisher<V> _execute(Core<V> core) {

		return new Publisher<V>() {

			String taskName = String.valueOf(Math.round(Math.random() * 1000));

			@Override
			public void subscribe(Subscriber<? super V> subscriber) {

				subscriber.onSubscribe(new Subscription() {

					volatile AtomicBoolean cancelled = new AtomicBoolean(false);

					@Override
					public void request(long l) {

						queue.add(new Task(taskName) {

							@Override
							public void run() {

								try {

									core.execute(subscriber, cancelled, logPrefix);
								} catch (Exception ex) {

									if (!cancelled.get())
										subscriber.onError(ex);
								}
							}
						});
					}

					@Override
					public void cancel() {

						log.debug("Subscriber canceled: " + taskName);
						cancelled.set(true);
					}
				});
			}
		};
	}

	private void createQueue() {

		queue = new ArrayBlockingQueue<>(capacity);

		Stream.iterate(1, i -> i + 1)
				.limit(numOfThreads)
				.map(i -> new Thread(() -> {

					while (true) {

						try {

							Runnable r = queue.take();
							log.debug("Took from queue: " + r);

							r.run();
						} catch (InterruptedException ex) {

							return;
						} catch (Exception ex) {

							log.debug("Exception in async executor", ex);
						}
					}
				}, "AsyncExecutor-" + i))
				.peek(t -> t.setDaemon(true))
				.forEach(Thread::start);
	}

	public interface Routine<T> {

		T execute(Context context) throws Exception;
	}

	public interface Routine2<T> {

		void execute(Context context, Function<T, Boolean> consumer) throws Exception;
	}

	@ToString
	@AllArgsConstructor
	private abstract static class Task implements Runnable {

		String logPrefix;
	}
}
