/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;

import java.util.function.Function;

@Slf4j
@Deprecated
public class LimittedRatePublisher<V, R> implements Publisher<R> {

	private Publisher<V> upstreamPublisher;
	private Function<V, Publisher<? extends R>> downstreamPublisherFactory;
	private int backpressure;
	private Subscription upstereamSubscription;
	private boolean canceled;
	private volatile long stillRequested;
	private final Object mutex = new Object();

	public LimittedRatePublisher(Publisher<V> upstreamPublisher, Function<V, Publisher<? extends R>> downstreamPublisherFactory, int backpressure) {

		this.upstreamPublisher = upstreamPublisher;
		this.downstreamPublisherFactory = downstreamPublisherFactory;
		this.backpressure = backpressure;
	}

	@Override
	public void subscribe(Subscriber<? super R> subscriber) {

		upstreamPublisher.subscribe(new BaseSubscriber<V>() {

			volatile boolean completed = false;
			volatile int inBuffer = 0;

			@Override
			protected void hookOnSubscribe(Subscription subscription) {

				upstereamSubscription = subscription;
				canceled = false;
			}

			@Override
			protected void hookOnNext(V value) {

				if (canceled)
					return;

				synchronized (mutex) {

					inBuffer ++;
				}

				downstreamPublisherFactory.apply(value)
						.subscribe(new BaseSubscriber<R>() {

							@Override
							protected void hookOnNext(R value) {

								subscriber.onNext(value);
							}

							@Override
							protected void hookOnComplete() {

								if (canceled)
									return;

								boolean shouldRequestMore = false;
								boolean shouldFireCompleted = false;

								synchronized (mutex) {

									inBuffer --;

									if (!completed && stillRequested > 0)
										shouldRequestMore = true;

									if (completed && inBuffer == 0)
										shouldFireCompleted = true;
								}

								if (shouldRequestMore)
									upstereamSubscription.request(1);

								if (shouldFireCompleted) {

									if (!canceled)
										subscriber.onComplete();
								}
							}
						});
			}

			@Override
			protected void hookOnComplete() {

				boolean shouldFireCompleted = false;
				synchronized (mutex) {

					completed = true;
					if (completed && inBuffer == 0)
						shouldFireCompleted = true;
				}

				if (shouldFireCompleted) {

					if (!canceled)
						subscriber.onComplete();
				}
			}
		});

		subscriber.onSubscribe(new Subscription() {

			@Override
			public void request(long n) {

				long itemsToRequest;

				synchronized (mutex) {

					itemsToRequest = Math.min(backpressure, n);
					stillRequested = n - itemsToRequest;
				}

				upstereamSubscription.request(itemsToRequest);
			}

			@Override
			public void cancel() {

				canceled = true;
			}
		});
	}
}
