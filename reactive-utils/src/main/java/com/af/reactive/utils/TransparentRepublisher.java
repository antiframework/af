/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class TransparentRepublisher<T> implements Publisher<T> {

	private Publisher<T> upstreamPublisher;
	private Interceptor<T> interceptor;
	protected String logPrefix;

	public TransparentRepublisher(Publisher<T> upstreamPublisher, Interceptor<T> interceptor, String logPrefix) {

		this.upstreamPublisher = upstreamPublisher;
		this.interceptor = interceptor;
		this.logPrefix = (logPrefix == null ? "" : logPrefix) + Math.round(Math.random() * 1000);
	}

	@Override
	public void subscribe(Subscriber<? super T> downstreamSubscriber) {

		log.debug(logPrefix + "# " + "Downstream subscriber subscribed, subscribing to upstream");

		CoreSubscriber<T> upstreamSubscriber = new CoreSubscriber<T>() {

			volatile AtomicBoolean cancelled = new AtomicBoolean(false);

			@Override
			public void onSubscribe(Subscription upstreamSubscription) {

				log.debug(logPrefix + "# " + "Upstream subscriber confirmed subscription, conforming subscription to downstream");

				Subscription downstreamSubscription = new Subscription() {

					@Override
					public void request(long l) {

						log.debug(logPrefix + "# " + "Downstream requested " + l + " items, requesting upstream");
						l = interceptor.onDownstreamRequest(l, logPrefix);

						upstreamSubscription.request(l);
					}

					@Override
					public void cancel() {

						log.debug(logPrefix + "# Downstream canceled. Canceling upstream");
						cancelled.set(true);
						upstreamSubscription.cancel();
						interceptor.onDownstreamCancel(logPrefix)
								.subscribe();
					}
				};

				downstreamSubscription = interceptor.onUpstreamSubscription(upstreamSubscription, downstreamSubscription, logPrefix);

				downstreamSubscriber.onSubscribe(downstreamSubscription);
			}

			@Override
			public void onNext(T t) {

				log.debug(logPrefix + "# " + "Upstream fired next " + t);

				if (cancelled.get()) {

					log.debug(logPrefix + "# " + "Upstream fired next, but cancelled. ignoring ...");
					return;
				}

				interceptor.onNext(t, logPrefix)
						.doOnNext(t1 -> {

							if (cancelled.get()) {

								log.debug(logPrefix + "# " + "Next is prepared for downstream but was cancelled. ignoring ... ");
								return;
							}

							log.debug(logPrefix + "# " + "Firing downstream next " + t);
							downstreamSubscriber.onNext(t1);
						})
						.doOnError(t2 -> {

							if (cancelled.get()) {

								log.debug(logPrefix + "# " + "Next caused error, error is prepared for downstream but was cancelled. ignoring ... ");
								return;
							}

							log.debug(logPrefix + "# " + "Firing downstream error on next");
							downstreamSubscriber.onError(t2);
						})
						.subscribe();
			}

			@Override
			public void onError(Throwable throwable) {

				if (cancelled.get()) {

					log.debug(logPrefix + "# " + "Upstream fired error, but cancelled. ignoring ...");
					return;
				}

				log.debug(logPrefix + "# " + "Upstream fired error");
				interceptor.onError(throwable, logPrefix)
						.doOnNext(t1 -> {

							if (cancelled.get()) {

								log.debug(logPrefix + "# " + "Upstream fired error, error prepared but cancelled. ignoring ...");
								return;
							}

							log.debug(logPrefix + "# " + "Firing downstream error");
							downstreamSubscriber.onError(t1);
						})
						.doOnError(t -> {

							if (cancelled.get()) {

								log.debug(logPrefix + "# " + "Upstream fired error on error, error prepared but cancelled. ignoring ...");
								return;
							}

							log.debug(logPrefix + "# " + "Firing downstream error on error");
							downstreamSubscriber.onError(t);
						})
						.subscribe();
			}

			@Override
			public void onComplete() {

				if (cancelled.get()) {

					log.debug(logPrefix + "# " + "Upstream fired complete, but cancelled. ignoring");
					return;
				}

				log.debug(logPrefix + "# " + "Upstream fired complete");
				interceptor.onComplete(logPrefix)
						.doOnError(downstreamSubscriber::onError)
						.subscribe(v -> {}, v -> {}, () -> {

							if (cancelled.get()) {

								log.debug(logPrefix + "# " + "Upstream fired complete, complete is prepared, but cancelled. ignoring");
								return;
							}

							log.debug(logPrefix + "# " + "Firing downstream complete");
							downstreamSubscriber.onComplete();
						});
			}

			@Override
			public Context currentContext() {

				log.debug(logPrefix + "# " + "Upstream asked for context, asking downstream.");

				if (downstreamSubscriber instanceof CoreSubscriber) {

					log.debug(logPrefix + "# " + "Downstream subscriber has context");

					return interceptor.onGetCurrentContext(((CoreSubscriber) downstreamSubscriber).currentContext(), logPrefix);
				}

				log.debug(logPrefix + "# " + "Downstream subscriber has no context");

				return interceptor.onGetCurrentContext(Context.empty(), logPrefix);
			}
		};

		interceptor.onDownstreamSubscribe(downstreamSubscriber, upstreamSubscriber, logPrefix)
				.doOnNext(us -> upstreamPublisher.subscribe(us))
				.doOnError(downstreamSubscriber::onError)
				.subscribe();

//		upstreamPublisher.subscribe(upstreamSubscriber);
	}

	public interface Interceptor<T> {

		default Mono<Subscriber<T>> onDownstreamSubscribe(Subscriber<? super T> downstreamSubscriber, CoreSubscriber<T> upstreamSubscriber, String publisherMarker) { return Mono.just(upstreamSubscriber); }
		default Subscription onUpstreamSubscription(Subscription upstreamSubscription, Subscription downstreamSubscription, String publisherMarker) { return downstreamSubscription; }

		default long onDownstreamRequest(long l, String publisherMarker) { return l; }
		default Mono<Void> onDownstreamCancel(String publisherMarker) { return Mono.empty(); }

		default Mono<T> onNext(T t, String publisherMarker) { return Mono.just(t); }
		default Mono<Throwable> onError(Throwable throwable, String publisherMarker) { return Mono.just(throwable); }
		default Mono<Void> onComplete(String publisherMarker) { return Mono.empty(); }

		default Context onGetCurrentContext(Context ctx, String publisherMarker) { return ctx; }
	}
}
