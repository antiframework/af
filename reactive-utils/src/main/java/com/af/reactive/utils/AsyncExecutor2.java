/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.Disposable;
import reactor.core.scheduler.Scheduler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Stream;

@SuppressWarnings("unused")
@Slf4j
public class AsyncExecutor2 implements Scheduler {

	private String name;
	private int numOfThreads;
	private int capacity;
	private long statisticsIntervalMs;
	private ArrayBlockingQueue<Runnable> queue;

	private volatile boolean disposed = false;
	private volatile long lastStatisticsTime = System.currentTimeMillis();

	public AsyncExecutor2(String name, int numOfThreads, int capacity, long statisticsIntervalMs) {

		this.name = name;
		this.numOfThreads = numOfThreads;
		this.capacity = capacity;
		this.statisticsIntervalMs = statisticsIntervalMs;

		createQueue();
	}

	@Override
	public Disposable schedule(Runnable task) {

		Disposable disposable = new Disposable() {

			private volatile boolean disposed = false;

			@Override
			public void dispose() {

				disposed = true;
			}

			@Override
			public boolean isDisposed() {
				return disposed;
			}
		};

		queue.add(() -> {

			if (disposable.isDisposed()) {

				log.debug("Queue: " + name + ". Async task is picked, but the task is disposed.");
				return;
			}

			task.run();
		});

		if (statisticsIntervalMs > 0 && System.currentTimeMillis() - lastStatisticsTime > statisticsIntervalMs)
			log.info("Queue: " + name + ". Queue capacity used " + queue.size() + " of " + capacity);

		return disposable;
	}

	@Override
	public Worker createWorker() {

		throw new RuntimeException("Not implemented");
	}

	@Override
	public void dispose() {

		disposed = true;
	}

	private void createQueue() {

		queue = new ArrayBlockingQueue<>(capacity);

		Stream.iterate(1, i -> i + 1)
				.limit(numOfThreads)
				.map(i -> new Thread(() -> {

					while (true) {

						try {

							Runnable r = queue.take();

							if (!disposed)
								r.run();
							else
								log.info("Queue: " + name + ". Task picked from queue but the executor is disposed. " + queue.size() + " tasks left.");

						} catch (InterruptedException ex) {

							return;
						} catch (Exception ex) {

							log.debug("Queue: " + name + ". Exception in async executor", ex);
						}
					}
				}, "Queue: " + name + ": " + i))
				.peek(t -> t.setDaemon(true))
				.forEach(Thread::start);
	}
}
