/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.util.context.Context;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class PressureControl<T> {

	private Publisher<T> upstreamPublisher;
	private int maxPressure;

	private volatile Subscription upstreamSubscription;
	private volatile Object mutex;
	private boolean completed;
	private int childIdIndex;
	private Map<Integer, Subscription> uncompletedChildSubscriptions;

	public PressureControl(Publisher<T> upstreamPublisher, int maxPressure) {

		this.upstreamPublisher = upstreamPublisher;
		this.maxPressure = maxPressure;

		mutex = new Object();

		completed = false;
		childIdIndex = 0;
		uncompletedChildSubscriptions = new HashMap<>();
	}

	public <R> Flux<R> flatMap(Function<? super T, ? extends Publisher<? extends R>> mapper) {

		//noinspection PublisherImplementation,Convert2Lambda
		return Flux.from(new Publisher<R>() {

			@Override
			public void subscribe(Subscriber<? super R> downstreamSubscriber) {

				upstreamPublisher.subscribe(new BaseSubscriber<T>() {

					@Override
					protected void hookOnSubscribe(Subscription us) {

						upstreamSubscription = us;

						downstreamSubscriber.onSubscribe(new Subscription() {

							@Override
							public void request(long n) {

								upstreamSubscription.request(Math.min(n, maxPressure - uncompletedChildSubscriptions.size()));
							}

							@Override
							public void cancel() {

								upstreamSubscription.cancel();
								cancelAllChildren();
							}
						});
					}

					@Override
					protected void hookOnNext(T value) {

						Publisher<? extends R> childPublisher = mapper.apply(value);

						childPublisher.subscribe(new BaseSubscriber<R>() {

							int childId;

							@Override
							protected void hookOnSubscribe(Subscription subscription) {

								synchronized (mutex) {

									uncompletedChildSubscriptions.put(childId = childIdIndex ++, subscription);
								}

								super.hookOnSubscribe(subscription);
							}

							@Override
							protected void hookOnNext(R value) {

								downstreamSubscriber.onNext(value);
							}

							@Override
							protected void hookOnComplete() {

								synchronized (mutex) {

									uncompletedChildSubscriptions.remove(childId);
									if (completed && uncompletedChildSubscriptions.isEmpty())
										downstreamSubscriber.onComplete();

									upstreamSubscription.request(1);
								}
							}

							@Override
							protected void hookOnError(Throwable throwable) {

								cancelAllChildren();
								downstreamSubscriber.onError(throwable);
							}

							@Override
							public Context currentContext() {

								if (downstreamSubscriber instanceof CoreSubscriber)
									return ((CoreSubscriber) downstreamSubscriber).currentContext();

								return Context.empty();
							}
						});
					}

					@Override
					protected void hookOnComplete() {

						synchronized (mutex) {

							completed = true;

							if (uncompletedChildSubscriptions.isEmpty())
								downstreamSubscriber.onComplete();
						}
					}

					@Override
					protected void hookOnError(Throwable throwable) {

						cancelAllChildren();
						downstreamSubscriber.onError(throwable);
					}

					@Override
					public Context currentContext() {
						if (downstreamSubscriber instanceof CoreSubscriber)
							return ((CoreSubscriber) downstreamSubscriber).currentContext();

						return Context.empty();
					}
				});
			}
		});
	}

	private void cancelAllChildren() {

		synchronized (mutex) {

			for (Subscription subscription : uncompletedChildSubscriptions.values())
				subscription.cancel();

			uncompletedChildSubscriptions = Collections.emptyMap();
		}
	}
}
