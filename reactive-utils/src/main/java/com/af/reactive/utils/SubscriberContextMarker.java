/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.utils;

import com.af.commons.BeanProxy;
import com.af.commons.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.lang.reflect.Method;

@SuppressWarnings("unused")
@Slf4j
@Deprecated
public class SubscriberContextMarker implements BeanProxy.MethodHandler {

	public static final String AF_CONTEXT_MARKER = "AF_CONTEXT_MARKER";

	public static String marker(Context context) {

		return context.getOrDefault(AF_CONTEXT_MARKER, "no marker");
	}

	@Override
	public boolean isEligible(Class<?> clazz, Method method) {

		return clazz.getAnnotation(Controller.class) != null && method.getAnnotation(RequestMapping.class) != null;
	}

	@Override
	public Object execute(Method method, BeanProxy.Caller caller) throws Throwable {

		if ((Mono.class.isAssignableFrom(method.getReturnType()) || Flux.class.isAssignableFrom(method.getReturnType()))) {

			log.warn("Proxying method for class " + method.getDeclaringClass().getName() + ", method: " + method.getName());

			@SuppressWarnings("unchecked")
			Publisher<?> targetPublisher = new TransparentRepublisher<>((Publisher<Object>) caller.call(), new TransparentRepublisher.Interceptor<Object>() {

				Pointer<Context> context = new Pointer<>(null);

				@Override
				public Context onGetCurrentContext(Context ctx, String publisherMarker) {

					return context.get();
				}

				@Override
				public Mono<Subscriber<Object>> onDownstreamSubscribe(Subscriber<? super Object> downstreamSubscriber, CoreSubscriber<Object> upstreamSubscriber, String publisherMarker) {

					Context context = downstreamSubscriber instanceof CoreSubscriber ? ((CoreSubscriber<?>) downstreamSubscriber).currentContext() : Context.empty();
					context = context.put(AF_CONTEXT_MARKER, "Context-" + (int)(Math.random() * 1000000));

					this.context.set(context);

					return Mono.just(upstreamSubscriber);
				}
			}, "Context-Marker");

			if (Mono.class.isAssignableFrom(method.getReturnType()))
				return Mono.from(targetPublisher);

			if (Flux.class.isAssignableFrom(method.getReturnType()))
				return Flux.from(targetPublisher);
		}

		return caller.call();
	}
}
