/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

public class Counter extends Pointer<Integer> {

	public Counter() {
		super(0);
	}

	public void inc() {

		set(get() + 1);
	}
}
