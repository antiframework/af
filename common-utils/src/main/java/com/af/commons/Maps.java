/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.util.HashMap;
import java.util.Map;

public class Maps {

	public static <K,V,E extends Exception> V computeIfAbsent(Map<K,V> map, K key, F<? super K, ? extends V, E> mappingFunction) throws E {

		V value = map.get(key);

		if (value == null) {

			value = mappingFunction.apply(key);
			map.put(key, value);
		}

		return value;
	}

	public interface F<T,R,E extends Exception> {
		R apply(T t) throws E;
	}

	public static <K,V> Builder<K,V> put(K key, V value) {

		Builder<K,V> b = new Builder<>();
		return b.put(key, value);
	}

	public static class Builder<K,V> {

		private final Map<K,V> map = new HashMap<>();

		public Builder<K,V> put(K key, V value) {
			map.put(key, value);
			return this;
		}

		public Map<K,V> build() {
			return map;
		}
	}
}
