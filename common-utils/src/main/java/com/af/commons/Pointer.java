/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pointer<T> {

	private T t;

	public T get() {
		return t;
	}

	public Pointer<T> set(T t) {
		this.t = t;
		return this;
	}
}
