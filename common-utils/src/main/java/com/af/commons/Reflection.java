/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Reflection {

	public static List<Method> listMethods(Class<?> clazz) {

		if (clazz.equals(Object.class))
			return Collections.emptyList();

		List<Method> methods = new ArrayList<>(Arrays.asList(clazz.getDeclaredMethods()));
		methods.addAll(listMethods(clazz.getSuperclass()));

		return methods;
	}

	public static List<Field> listFields(Class<?> clazz) {

		if (clazz.equals(Object.class))
			return Collections.emptyList();

		List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
		fields.addAll(listFields(clazz.getSuperclass()));

		return fields;
	}

	public static Object getFieldValue(Field field, Object o) throws IllegalAccessException {

		field.setAccessible(true);

		return field.get(o);
	}
}
