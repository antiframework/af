/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.util.concurrent.Callable;
import java.util.function.Function;

public class Wrap {

	public static <T,EX extends Exception> T wrap(Callable<? extends T> callable, Function<? super Exception, EX> exSupplier) throws EX {

		try {

			return callable.call();
		} catch (Exception ex) {

			throw exSupplier.apply(ex);
		}
	}
}
