/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

public class Strings {

	public static String trim(String s, int length) {

		return s.length() > length ? s.substring(0, length - 3) + "..." : s;
	}
}
