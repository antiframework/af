/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Deprecated
public class BeanProxy implements BeanPostProcessor {

	private List<MethodHandler> methodHandlers;

	public BeanProxy(MethodHandler ... methodHandlers) {

		this.methodHandlers = Arrays.asList(methodHandlers);
		Collections.reverse(this.methodHandlers);
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

		Class<?> clazz = bean.getClass();
		List<Method> declaredMethods = Reflection.listMethods(bean.getClass());
		for (Method method : declaredMethods) {

			for (MethodHandler methodHandler : methodHandlers) {

				if (methodHandler.isEligible(clazz, method))
					return createProxy(clazz, bean);
			}
		}

		return bean;
	}

	private Object createProxy(Class<?> clazz, Object bean) {

		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(clazz);

		enhancer.setCallback((MethodInterceptor) (proxyObject, originalMethod, args, methodProxy) -> {

			Caller caller = () -> methodProxy.invoke(bean, args);

			for (MethodHandler methodHandler : methodHandlers) {

				if (methodHandler.isEligible(clazz, originalMethod))
					caller = createSubCaller(originalMethod, methodHandler, caller);
			}

			return caller.call();
		});

		return enhancer.create();
	}

	private Caller createSubCaller(Method method, MethodHandler methodHandler, Caller caller) {

		return () -> methodHandler.execute(method, caller);
	}

	public interface MethodHandler {

		boolean isEligible(Class<?> clazz, Method m);
		Object execute(Method method, Caller caller) throws Throwable;
	}

	public interface Caller {

		Object call() throws Throwable;
	}
}
