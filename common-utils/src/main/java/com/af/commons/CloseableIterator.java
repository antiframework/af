/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.util.Iterator;

public interface CloseableIterator<T> extends AutoCloseable {

	boolean hasNext() throws Exception;
	T next() throws  Exception;
}
