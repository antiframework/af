/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class EnhancerUtils {

	public static <T> T enhance(Class<T> clazz, Interceptor interceptor) {

		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(clazz);

		enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) ->
				interceptor.intercept(method, args, proxy));

		//noinspection unchecked
		return (T) enhancer.create();
	}

	public interface Interceptor {

		Object intercept(Method originalMethod, Object[] args, MethodProxy methodProxy) throws Throwable;
	}
}
