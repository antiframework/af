/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.sql.Timestamp;
import java.util.Date;

public class Dates {

	public static Timestamp toTimestamp(Date d) {

		return d == null ? null : new Timestamp(d.getTime());
	}
}
