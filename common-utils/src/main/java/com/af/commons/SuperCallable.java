package com.af.commons;

public interface SuperCallable {

	Object call() throws Throwable;
}
