/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.commons;

import java.util.concurrent.Callable;
import java.util.function.Function;

public class Uncheck {

	public static <T> T uncheck(Callable<? extends T> callable) {

		try {

			return callable.call();
		} catch (Exception ex) {

			throw new RuntimeException(ex);
		}
	}

	public static <T> T uncheck(Callable<? extends T> callable, Function<? super Exception, ? extends RuntimeException> exSupplier) {

		try {

			return callable.call();
		} catch (Exception ex) {

			throw exSupplier.apply(ex);
		}
	}

	public static void uncheck(Runner runnable) {

		try {

			runnable.run();
		} catch (Exception ex) {

			throw new RuntimeException(ex);
		}
	}

	public interface Runner {
		void run() throws Exception;
	}
}
