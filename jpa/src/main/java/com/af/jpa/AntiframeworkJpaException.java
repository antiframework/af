/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa;

public class AntiframeworkJpaException extends Exception {

	public AntiframeworkJpaException(String message) {
		super(message);
	}

		public AntiframeworkJpaException(String message, Throwable cause) {
		super(message, cause);
	}
}
