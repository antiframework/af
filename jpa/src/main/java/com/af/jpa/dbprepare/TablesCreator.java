/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.dbprepare;

import com.af.jpa.queries.JPAUtil;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.sql.*;

@Slf4j
public class TablesCreator {

	public void createTable(Connection conn, Class<?> clazz) throws SQLException {

		String query = createQuery(clazz);

		log.debug("Creating table: " + query);

		try (PreparedStatement stmt = conn.prepareStatement(query)) {

			stmt.executeUpdate();
		}


//		TransactionTemplate txTemplate = new TransactionTemplate(transactionManager);
//		txTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
//
//		TransactionStatus trx = transactionManager.getTransaction(TransactionDefinition.withDefaults());
//
//		trx.
//
//		txTemplate.executeWithoutResult(trxStatus -> {
//
//			trxStatus.
//		});
	}

	private String createQuery(Class<?> clazz) {

		StringBuilder sb = new StringBuilder("create table ");
		sb.append(tableName(clazz));
		sb.append(" (");

		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {

			Field field = fields[i];
			String dbType = dbType(field.getType());

			if (dbType != null) {

				String columnName;
				if (field.getAnnotation(Column.class) != null)
					columnName = field.getAnnotation(Column.class).name();
				else
					columnName = JPAUtil.toSnake(field.getName());

				sb.append(columnName);
				sb.append(" ");

				sb.append(dbType);

				if (field.getAnnotation(Id.class) != null)
					sb.append(" primary key not null ");

				if (field.getAnnotation(GeneratedValue.class) != null)
					sb.append(" identity ");

				if (i < fields.length - 1)
					sb.append(", ");
			}
		}

		sb.append(" )");
		return sb.toString();
	}

	private String dbType(Class<?> type) {

		if (type.isAssignableFrom(String.class))
			return "varchar(100)";

		if (type.isAssignableFrom(Long.class))
			return "bigint";

		if (type.isAssignableFrom(Integer.class))
			return "int";

		if (type.isAssignableFrom(Double.class))
			return "numeric";

		if (type.isAssignableFrom(Date.class) || type.isAssignableFrom(Timestamp.class))
			return "timestamp";

		if (type.isAssignableFrom(Boolean.class))
			return "int";

		log.debug("unsupported column type: " + type.getSimpleName());

		return null;
	}

	private String tableName(Class<?> clazz) {

		Table tableAnnotation = clazz.getAnnotation(Table.class);
		if (tableAnnotation != null)
			return tableAnnotation.name();

		return JPAUtil.toSnake(clazz.getSimpleName());
	}
}
