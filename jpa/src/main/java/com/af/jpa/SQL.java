/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa;

import com.af.commons.CloseableIterator;
import com.af.commons.Maps;
import com.af.commons.Reflection;
import com.af.commons.Strings;
import com.af.jpa.dialects.Dialect;
import com.af.jpa.queries.*;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class SQL {

	private final Map<Class<?>, CreateQuery> createQueries = new ConcurrentHashMap<>();
	private final Map<Class<?>, UpdateQuery> updateQueries = new ConcurrentHashMap<>();
	private final Map<Class<?>, FetchQuery<?>> fetchQueries = new ConcurrentHashMap<>();
	private final Map<Class<?>, DeleteQuery> deleteQueries = new ConcurrentHashMap<>();

	private final Dialect dialect;

	public SQL(Dialect dialect) {
		this.dialect = dialect;
	}

	public void save(Connection conn, Object entity) throws AntiframeworkJpaException {

		try {

			CreateQuery query = Maps.computeIfAbsent(createQueries, entity.getClass(), c -> new CreateQuery(c, dialect));

			boolean update = false;
			if (query.getIdField() != null && Reflection.getFieldValue(query.getIdField(), entity) != null) {

				query = Maps.computeIfAbsent(updateQueries, entity.getClass(), c -> new UpdateQuery(c, dialect));
				update = true;
			}

			try (PreparedStatement stmt = conn.prepareStatement(query.getQuery())) {

				List<Object> values = query.setParameters(stmt, entity);

				log.debug((update ? "Updating" : "Creating") + " entity: query: " + query.getQuery() + ", values: {" + values.stream().map(v -> v == null ? "null" : v.toString()).collect(Collectors.joining(",")) + "}");

				stmt.executeUpdate();

				if (!update && query.isHasGeneratedId()) {

					try (final PreparedStatement stmt2 = conn.prepareStatement(dialect.getIdentityFunction())) {

						try (ResultSet rset = stmt2.executeQuery()) {

							if (rset.next()) {

								Object newId = JPAUtil.getValue(query.getIdField().getType(), rset, 1);
								query.setId(entity, newId);

								log.debug("Creating entity: query: " + query.getQuery() + ", values: (" + values.stream().map(v -> v == null ? "null" : v.toString()).collect(Collectors.joining(") ,(")) + ") Assigned id: " + newId);
							}
						}
					}
				}
			}
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Save failed, entity: " + Strings.trim(entity.toString(), 50), ex);
		}
	}

	public <T> Optional<T> fetchById(Connection conn, EntityConstructor<T> entityConstructor, Object id) throws AntiframeworkJpaException {

		try {

			FetchQuery<?> fetchQuery = fetchQueries.computeIfAbsent(entityConstructor.getEntityClass(), FetchQuery::new);

			log.debug(fetchQuery.getByIdQuery() + ", parameter: " + id);

			try (PreparedStatement stmt = conn.prepareStatement(fetchQuery.getByIdQuery())) {

				JPAUtil.setParameter(stmt, 1, id);
				try (ResultSet rset = stmt.executeQuery()) {

					if (rset.next())
						return Optional.of(entityConstructor.createEntity(rset));
				}
			}

			return Optional.empty();
		} catch (AntiframeworkJpaException ex) {

			throw ex;
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Fetch by id failed, id: " + id + ", class: " + entityConstructor.getEntityClass().getCanonicalName(), ex);
		}
	}

	public <T> CloseableIterator<T> fetchAll(Connection conn, EntityConstructor<T> entityConstructor, String query, List<?> params) throws AntiframeworkJpaException {

		try {

			log.debug(query + ", params: (" + params.stream().map(o -> o == null ? "null" : o.toString()).collect(Collectors.joining("), (")) + ")");

			PreparedStatement stmt = conn.prepareStatement(query);

			int index = 1;
			for (Object param : params)
				JPAUtil.setParameter(stmt, index ++, param);

			ResultSet rset = stmt.executeQuery();

			return new CloseableIterator<T>() {

				@Override
				public void close() throws Exception {

					rset.close();
					stmt.close();
				}

				@Override
				public boolean hasNext() throws Exception {

					return rset.next();
				}

				@Override
				public T next() throws Exception {

					return entityConstructor.createEntity(rset);
				}
			};
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Fetch error, query: " + query, ex);
		}
	}

	public int update(Connection conn, String query, List<?> params) throws AntiframeworkJpaException {

		try {

			log.debug(query + ", params: (" + params.stream().map(o -> o == null ? "null" : o.toString()).collect(Collectors.joining("), (")) + ")");

			PreparedStatement stmt = conn.prepareStatement(query);

			int index = 1;
			for (Object param : params)
				JPAUtil.setParameter(stmt, index ++, param);

			return stmt.executeUpdate();
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Fetch error", ex);
		}
	}

	public <T> Optional<T> fetchOne(Connection conn, EntityConstructor<T> entityConstructor, String query, List<?> params) throws AntiframeworkJpaException {

		try {

			log.debug(query + ", params: (" + params.stream().map(o -> o == null ? "null" : o.toString()).collect(Collectors.joining("), (")) + ")");

			PreparedStatement stmt = conn.prepareStatement(query);

			int index = 1;
			for (Object param : params)
				JPAUtil.setParameter(stmt, index ++, param);

			ResultSet rset = stmt.executeQuery();

			if (rset.next())
				return Optional.of(entityConstructor.createEntity(rset));

			return Optional.empty();
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Fetch error", ex);
		}
	}

	public int delete(Connection conn, Class<?> clazz, Object id) throws AntiframeworkJpaException {

		try {

			DeleteQuery query = Maps.computeIfAbsent(deleteQueries, clazz, c -> new DeleteQuery(clazz));

			log.debug(query.getQuery() + ", params: (" + id + ")");

			PreparedStatement stmt = conn.prepareStatement(query.getQuery());
			JPAUtil.setParameter(stmt, 1, id);

			return stmt.executeUpdate();
		} catch (SQLException ex) {

			throw new AntiframeworkJpaException("Delete of entity failed, class: " + clazz + ", id: " + id);
		}
	}
}
