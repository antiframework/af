/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import java.sql.ResultSet;

public class LongEntityConstructor implements EntityConstructor<Long> {

	@Override
	public Class<Long> getEntityClass() {
		return Long.class;
	}

	@Override
	public Long createEntity(ResultSet rset) throws Exception {

		long value = rset.getLong(1);

		return rset.wasNull() ? null : value;
	}
}
