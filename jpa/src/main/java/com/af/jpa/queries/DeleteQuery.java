/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.commons.Reflection;
import com.af.jpa.AntiframeworkJpaException;
import lombok.Getter;

import javax.persistence.Id;
import java.lang.reflect.Field;

public class DeleteQuery {

	private Class<?> entityClass;
	@Getter
	private String query;

	public DeleteQuery(Class<?> entityClass) throws AntiframeworkJpaException {
		this.entityClass = entityClass;

		createQuery();
	}

	private void createQuery() throws AntiframeworkJpaException {

		StringBuilder sb = new StringBuilder("delete from ");
		sb.append(JPAUtil.tableName(entityClass));
		sb.append(" where ");

		Field idField = Reflection.listFields(entityClass).stream()
				.filter(f -> f.getAnnotation(Id.class) != null)
				.findFirst()
				.orElseThrow(() -> new AntiframeworkJpaException("delete query can't be created for entity without id, class: " + entityClass.getCanonicalName()));

		if (!JPAUtil.isAssignable(idField.getType()))
			throw new AntiframeworkJpaException("delete query can't be created for entity with unsupported id, class: " + entityClass.getCanonicalName() + ", field: " + idField.getType().getCanonicalName());

		sb.append(JPAUtil.columnNameByField(idField));
		sb.append(" = ?");

		query = sb.toString();
	}
}