/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import java.sql.ResultSet;

public interface EntityConstructor<T> {

	Class<T> getEntityClass();
	T createEntity(ResultSet rset) throws Exception;
}
