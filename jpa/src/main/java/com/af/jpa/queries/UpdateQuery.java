/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.commons.Reflection;
import com.af.jpa.AntiframeworkJpaException;
import com.af.jpa.dialects.Dialect;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class UpdateQuery extends CreateQuery {

	public UpdateQuery(Class<?> entityClass, Dialect dialect) throws AntiframeworkJpaException {
		super(entityClass, dialect);
	}

	@Override
	protected void createQuery() {

		StringBuilder sb = new StringBuilder("update ");
		sb.append(JPAUtil.tableName(entityClass));
		sb.append(" set ");

		for (Field field : fields) {

			if (JPAUtil.isAssignable(field.getType())) {

				GeneratedValue generatedAnn = field.getAnnotation(GeneratedValue.class);
				if (generatedAnn == null) {

					if (field.getAnnotation(Id.class) == null) {

						Column columnAnn = field.getAnnotation(Column.class);
						if (columnAnn == null || columnAnn.insertable()) {

							if (field.getAnnotation(CreationTimestamp.class) == null) {
								sb.append(JPAUtil.columnNameByField(field));
								sb.append("=");

								if (field.getAnnotation(UpdateTimestamp.class) != null)
									sb.append(dialect.getTimestampFunction());
								else
									sb.append("?");

								sb.append(", ");
							}
						}
					}
				}
			}
		}

		sb.delete(sb.length() - 2, sb.length());

		sb.append(" where ");

		sb.append(JPAUtil.columnNameByField(idField));
		sb.append(" = ?");

		query = sb.toString();
	}

	public List<Object> setParameters(PreparedStatement stmt, Object o) throws AntiframeworkJpaException {

		return setParameters(stmt, o, true);
	}
}
