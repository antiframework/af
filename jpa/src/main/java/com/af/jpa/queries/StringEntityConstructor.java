/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import java.sql.ResultSet;

public class StringEntityConstructor implements EntityConstructor<String> {

	@Override
	public Class<String> getEntityClass() {
		return String.class;
	}

	@Override
	public String createEntity(ResultSet rset) throws Exception {

		return rset.getString(1);
	}
}
