/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.commons.Reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class ReflectionEntityConstructor<T> implements EntityConstructor<T> {

	private Class<T> entityClass;
	private String columnNamePrefix;
	private Map<String, EntityConstructor<?>> subs;
	private Set<Class<?>> supportedTypes;
	private volatile Set<String> rsetColumns;
	private volatile Map<String, Integer> rsetColumnTypes;
	private List<Field> fields;
	private Map<String,String> additionalColumns;

	public ReflectionEntityConstructor(Class<T> entityClass, String columnNamePrefix) {

		this.entityClass = entityClass;
		this.columnNamePrefix = columnNamePrefix;

		subs = new HashMap<>();
		supportedTypes = new HashSet<>();
		additionalColumns = new HashMap<>();

		prepare();
	}

	public ReflectionEntityConstructor(Class<T> entityClass) {

		this(entityClass, null);
	}

	public ReflectionEntityConstructor<T> addSub(String fieldName, EntityConstructor<?> sub) {

		subs.put(fieldName, sub);
		supportedTypes.add(sub.getEntityClass());

		return this;
	}

	public ReflectionEntityConstructor<T> addAdditionalColumn(String fieldName, String columnName) {

		additionalColumns.put(fieldName, columnName);

		return this;
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}

	public T createEntity(ResultSet rset) throws Exception {

		T entity = entityClass.newInstance();

		initRsetMetadata(rset);

		for (Field field : fields) {

			if (!JPAUtil.isAssignable(field.getType()) && !supportedTypes.contains(field.getType()))
				continue;

			Object value = null;

			EntityConstructor<?> sub = subs != null ? subs.get(field.getName()) : null;
			if (sub != null)
				value = sub.createEntity(rset);
			else {

				String columnName = additionalColumns.get(field.getName());

				if (columnName == null)
					columnName = JPAUtil.columnNameByField(field);

				if (columnNamePrefix != null)
					columnName = columnNamePrefix + columnName;

				columnName = columnName.toLowerCase();

				if (rsetColumns.contains(columnName))
					value = JPAUtil.getValue(field.getType(), rset, columnName, rsetColumnTypes.get(columnName));
			}

			if (value != null)
				field.set(entity, value);
		}

		return entity;
	}

	private void initRsetMetadata(ResultSet rset) throws SQLException {

		if (rsetColumns == null) {

			HashSet<String> hashSet = new HashSet<>();
			Map<String, Integer> types = new HashMap<>();

			ResultSetMetaData metaData = rset.getMetaData();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {

				String columnName = metaData.getColumnLabel(i).toLowerCase();

				hashSet.add(columnName);
				types.put(columnName, metaData.getColumnType(i));
			}

			rsetColumns = hashSet;
			rsetColumnTypes = types;
		}
	}

	private void prepare() {

		fields = Reflection.listFields(entityClass)
				.stream()
//				.filter(f -> JPAUtil.isAssignable(f.getType()))
				.filter(f -> !Modifier.isFinal(f.getModifiers()))
				.peek(f -> f.setAccessible(true))
				.collect(Collectors.toList());
	}
}
