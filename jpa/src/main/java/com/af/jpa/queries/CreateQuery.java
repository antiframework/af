/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.commons.Reflection;
import com.af.jpa.AntiframeworkJpaException;
import com.af.jpa.dialects.Dialect;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class CreateQuery {

	protected Class<?> entityClass;
	protected Dialect dialect;
	@Getter
	protected String query;
	protected List<Field> fields;
	@Getter
	protected Field idField;
	@Getter
	private boolean hasGeneratedId = false;

	public CreateQuery(Class<?> entityClass, Dialect dialect) throws AntiframeworkJpaException {

		this.entityClass = entityClass;
		this.dialect = dialect;

		prepare();
	}

	public List<Object> setParameters(PreparedStatement stmt, Object o) throws AntiframeworkJpaException {

		return setParameters(stmt, o, false);
	}

	public void setId(Object o, Object id) throws IllegalAccessException {

		boolean accessible = idField.isAccessible();
		idField.setAccessible(true);
		idField.set(o, id);
		idField.setAccessible(accessible);
	}

	protected List<Object> setParameters(PreparedStatement stmt, Object o, boolean isUpdate) throws AntiframeworkJpaException {

		try {

			int index = 1;
			List<Object> values = new ArrayList<>();

			for (Field field : fields) {

				if (JPAUtil.isAssignable(field.getType())) {

					GeneratedValue generatedAnn = field.getAnnotation(GeneratedValue.class);
					if (generatedAnn == null) {

						Column columnAnn = field.getAnnotation(Column.class);
						if (columnAnn == null || columnAnn.insertable()) {

							if (field.getAnnotation(CreationTimestamp.class) == null && field.getAnnotation(UpdateTimestamp.class) == null && (field.getAnnotation(Id.class) == null || !isUpdate)) {

								Object value = Reflection.getFieldValue(field, o);

								if (value instanceof Enum)
									value = ((Enum<?>) value).name();

								JPAUtil.setParameter(stmt, index, value);
								values.add(value);

								index ++;
							}
						}
					}
				}
			}

			if (isUpdate) {

				Object value = Reflection.getFieldValue(idField, o);

				JPAUtil.setParameter(stmt, index, value);
				values.add(value);
			}

			return values;
		} catch (AntiframeworkJpaException ex) {

			throw ex;
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Unable to set statement parameter: class: " + entityClass, ex);
		}
	}

	private void prepare() throws AntiframeworkJpaException {

		fields = Reflection.listFields(entityClass);

		prepareIds();
		createQuery();
	}

	private void prepareIds() throws AntiframeworkJpaException {

		for (Field field : fields) {

			if (JPAUtil.isAssignable(field.getType())) {

				GeneratedValue generatedAnn = field.getAnnotation(GeneratedValue.class);
				if (generatedAnn != null)
					hasGeneratedId = true;

				if (field.getAnnotation(Id.class) != null) {

					if (field.getType().isPrimitive())
						throw new AntiframeworkJpaException("Entity id field can't be primitive");

					idField = field;
				}
			}
		}
	}

	protected void createQuery() {

		StringBuilder sb = new StringBuilder("insert into ");
		sb.append(JPAUtil.tableName(entityClass));
		sb.append("(");

		fields = Reflection.listFields(entityClass);

		for (Field field : fields) {

			if (JPAUtil.isAssignable(field.getType())) {

				GeneratedValue generatedAnn = field.getAnnotation(GeneratedValue.class);
				if (generatedAnn == null) {

					Column columnAnn = field.getAnnotation(Column.class);
					if (columnAnn == null || columnAnn.insertable()) {

						sb.append(JPAUtil.columnNameByField(field));
						sb.append(", ");
					}
				}
			}
		}

		sb.delete(sb.length() - 2, sb.length());
		sb.append(") values(");

		for (Field field : fields) {

			if (JPAUtil.isAssignable(field.getType())) {

				GeneratedValue generatedAnn = field.getAnnotation(GeneratedValue.class);
				if (generatedAnn == null) {

					Column columnAnn = field.getAnnotation(Column.class);
					if (columnAnn == null || columnAnn.insertable()) {

						if (field.getAnnotation(CreationTimestamp.class) != null || field.getAnnotation(UpdateTimestamp.class) != null)
							sb.append(dialect.getTimestampFunction()).append(", ");
						else
							sb.append("?, ");
					}
				}
			}
		}

		sb.delete(sb.length() - 2, sb.length());
		sb.append(") ");

		query = sb.toString();
	}
}
