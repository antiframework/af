/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.commons.Reflection;
import lombok.Getter;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;

public class FetchQuery<T> {

	private Class<T> entityClass;
	@Getter
	private String byIdQuery;
	private Field idField;

	public FetchQuery(Class<T> entityClass) {

		this.entityClass = entityClass;
		prepare();
	}

	private void prepare() {

		createQuery();
	}

	private void createQuery() {

		StringBuilder sb = new StringBuilder("select ");

		List<Field> fields = Reflection.listFields(entityClass)
				.stream()
				.filter(f -> JPAUtil.isAssignable(f.getType()))
				.filter(f -> !Modifier.isFinal(f.getModifiers()))
				.collect(Collectors.toList());

		for (Field field : fields) {

			if (field.getAnnotation(Id.class) != null)
				idField = field;

			sb.append(JPAUtil.columnNameByField(field));
			sb.append(", ");
		}

		sb.delete(sb.length() - 2, sb.length());

		sb.append(" from ");
		sb.append(JPAUtil.tableName(entityClass));

		if (idField != null) {

			sb.append(" where ");
			sb.append(JPAUtil.columnNameByField(idField));
			sb.append(" = ?");
		}

		byIdQuery = sb.toString();
	}
}
