/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.queries;

import com.af.jpa.AntiframeworkJpaException;

import javax.persistence.Column;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.Date;

public class JPAUtil {

	private static final Class<?>[] ASSIGNABLE_TYPES = new Class[] {Number.class, String.class, Date.class, Boolean.class, Enum.class};

	public static String tableName(Class<?> clazz) {

		Table tableAnnotation = clazz.getAnnotation(Table.class);
		if (tableAnnotation != null)
			return tableAnnotation.name();

		return JPAUtil.toSnake(clazz.getSimpleName());
	}

	public static String toSnake(String s) {

		int originalLength = s.length();
		int targetLength = 0;
		char[] target = new char[originalLength * 2];
		for (int i = 0; i < originalLength; i++) {

			if (Character.isUpperCase(s.charAt(i))) {

				target[targetLength++] = '_';
				target[targetLength++] = Character.toLowerCase(s.charAt(i));
			} else
				target[targetLength++] = s.charAt(i);
		}

		return String.valueOf(target, 0, targetLength);
	}

	public static String columnNameByField(Field field) {

		Column columnAnn = field.getAnnotation(Column.class);
		if (columnAnn == null || columnAnn.name().isEmpty())
			return JPAUtil.toSnake(field.getName());

		return columnAnn.name();
	}

	public static void setParameter(PreparedStatement stmt, int index, Object value) throws SQLException, AntiframeworkJpaException {

		if (value == null)
			stmt.setNull(index, Types.NULL);
		else if (value instanceof String)
			stmt.setString(index, (String) value);
		else if (value instanceof Long)
			stmt.setLong(index, (Long) value);
		else if (value instanceof Integer)
			stmt.setLong(index, (Integer) value);
		else if (value instanceof Double)
			stmt.setDouble(index, (Double) value);
		else if (value instanceof Date)
			stmt.setTimestamp(index, (Timestamp) value);
		else if (value instanceof Boolean)
			stmt.setBoolean(index, (Boolean) value);
		else
			throw new AntiframeworkJpaException("unsupported column type: " + value.getClass().getName());
	}

	@SuppressWarnings("unchecked")
	public static <T> T getValue(Class<T> type, ResultSet rset, String columnName, Integer sqlType) throws SQLException, AntiframeworkJpaException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

		if (String.class.isAssignableFrom(type))
			return (T) rset.getString(columnName);

		if (Enum.class.isAssignableFrom(type))
			return (T)  Enum.class.getMethod("valueOf", Class.class, String.class).invoke(null, type, rset.getString(columnName));

		if (Long.class.isAssignableFrom(type)) {

			long l = rset.getLong(columnName);
			return rset.wasNull() ? null : (T) new Long(l);
		}

		if (Integer.class.isAssignableFrom(type)) {

			int i = rset.getInt(columnName);
			return rset.wasNull() ? null : (T) new Integer(i);
		}

		if (Double.class.isAssignableFrom(type)) {

			double i = rset.getDouble(columnName);
			return rset.wasNull() ? null : (T) new Double(i);
		}

		if (Boolean.class.isAssignableFrom(type)) {

			if (sqlType == Types.BOOLEAN || sqlType == Types.BIT) {

				boolean i = rset.getBoolean(columnName);
				return rset.wasNull() ? null : (T) Boolean.valueOf(i);
			}

			if (sqlType == Types.INTEGER) {

				int i = rset.getInt(columnName);
				return rset.wasNull() ? null : (T) (i != 0 ? Boolean.TRUE : Boolean.FALSE);
			}

			if (sqlType == Types.VARCHAR || sqlType == Types.CHAR) {

				String s = rset.getString(columnName);

				return (T) Boolean.valueOf(s != null && ((s = s.toLowerCase()).equals("true") || s.startsWith("y")));
			}
		}

		if (Timestamp.class.isAssignableFrom(type))
			return (T) rset.getTimestamp(columnName);
		if (Date.class.isAssignableFrom(type))
			return (T) rset.getTimestamp(columnName);

		throw new AntiframeworkJpaException("unsupported column type: " + type.getName());
	}

	@SuppressWarnings("unchecked")
	public static <T> T getValue(Class<T> type, ResultSet rset, int index) throws SQLException, AntiframeworkJpaException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

		if (String.class.isAssignableFrom(type))
			return (T) rset.getString(index);

		if (Enum.class.isAssignableFrom(type))
			return (T)  Enum.class.getMethod("valueOf", Class.class, String.class).invoke(null, type, rset.getString(index));

		if (Long.class.isAssignableFrom(type)) {

			long l = rset.getLong(index);
			return rset.wasNull() ? null : (T) new Long(l);
		}

		if (Integer.class.isAssignableFrom(type)) {

			int i = rset.getInt(index);
			return rset.wasNull() ? null : (T) new Integer(i);
		}

		if (Double.class.isAssignableFrom(type)) {

			double i = rset.getDouble(index);
			return rset.wasNull() ? null : (T) new Double(i);
		}

		if (Boolean.class.isAssignableFrom(type)) {

			boolean i = rset.getBoolean(index);
			return rset.wasNull() ? null : (T) Boolean.valueOf(i);
		}

		if (Timestamp.class.isAssignableFrom(type))
			return (T) rset.getTimestamp(index);
		if (Date.class.isAssignableFrom(type))
			return (T) rset.getTimestamp(index);

		throw new AntiframeworkJpaException("unsupported column type: " + type.getName());
	}

	public static boolean isAssignable(Class<?> clazz) {

		for (Class<?> type : ASSIGNABLE_TYPES) {

			if (type.isAssignableFrom(clazz))
				return true;
		}

		return false;
	}
}
