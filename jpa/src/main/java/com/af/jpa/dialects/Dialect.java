/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.dialects;

public interface Dialect {

	String getTimestampFunction();
	String getIdentityFunction();
	String getRownumFunction();
}
