/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.dialects;

public class OracleDialect implements Dialect {

	@Override
	public String getTimestampFunction() {

		return "sysdate";
	}

	@Override
	public String getIdentityFunction() {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public String getRownumFunction() {
		return "rownum";
	}
}
