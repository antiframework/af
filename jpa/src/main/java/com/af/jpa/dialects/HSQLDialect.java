/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.dialects;

public class HSQLDialect implements Dialect {

	@Override
	public String getTimestampFunction() {
		return "current_timestamp";
	}

	@Override
	public String getIdentityFunction() {
		return "CALL IDENTITY()";
	}

	@Override
	public String getRownumFunction() {
		return "rownum()";
	}
}
