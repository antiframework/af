/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.jpa.dialects;

public class PostgresqlDialect implements Dialect {

	@Override
	public String getTimestampFunction() {
		return "current_timestamp";
	}

	@Override
	public String getIdentityFunction() {
		return "select lastval()";
	}

	@Override
	public String getRownumFunction() {
		throw new RuntimeException("not implemented");
	}
}
