/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.web;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.af.reactive.utils.SubscriberContextMarker.AF_CONTEXT_MARKER;

public class LogMarker implements WebFilter {

	private static final String AF_CORRELATION_ID = "AF-CorrelationId";

	private final Supplier<String> markGenerator;

	public LogMarker() {

		this(() -> "Context-" + (int)(Math.random() * 1000000));
	}

	public LogMarker(Supplier<String> markGenerator) {
		this.markGenerator = markGenerator;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {

		ServerHttpRequest request = exchange.getRequest();
		List<String> headers = request.getHeaders().get(AF_CORRELATION_ID);

		return chain.filter(exchange)
				.subscriberContext(ctx -> ctx.put(AF_CONTEXT_MARKER, headers == null || headers.isEmpty() ? markGenerator.get() : headers.get(0)));
	}

	public static <T> Mono<T> mark(Consumer<String> c) {

		return Mono.create(sync -> {

			c.accept(sync.currentContext().getOrDefault(AF_CONTEXT_MARKER, "none"));
			sync.success();
		});
	}

	public static <T> Mono<T> mark(Consumer<String> c, T valueToFire) {

		return mark(c)
				.then(Mono.just(valueToFire));
	}

	public static Context markContext(Context ctx, Supplier<String> markGenerator) {

		return ctx.put(AF_CONTEXT_MARKER, markGenerator.get());
	}

	public static String mark(Context ctx) {

		return ctx.getOrDefault(AF_CONTEXT_MARKER, "none");
	}
}
