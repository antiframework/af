/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.web;

import com.af.commons.Reflection;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.constraints.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class RecursiveValidator implements Validator {

	public static final List<Class<?>> CLASSES_TO_SKIP = Arrays.asList(String.class, Number.class, Boolean.class, Enum.class, Date.class, Map.class, Collection.class);

	public RecursiveValidator(javax.validation.Validator validator) {
		this.validator = validator;
	}

	private javax.validation.Validator validator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {

		try {

			ArrayList<ConstraintViolationWrapper> violations = new ArrayList<>();

			validateObject(null, violations, target);

			if (!violations.isEmpty()) {

				String message = violationsToMessage(violations);
				throw new HttpMessageNotReadableException(message);
			}

		} catch (HttpMessageNotReadableException ex) {

			throw ex;
		} catch (Exception ex) {

			throw new HttpMessageNotReadableException("Error while validating", ex);
		}
	}

	private void validateObject(String path, ArrayList<ConstraintViolationWrapper> violations, Object o) throws IllegalAccessException {

		violations.addAll(validator.validate(o).stream().map(v -> ConstraintViolationWrapper.builder()
				.path((path == null ? "" : path + ".") + v.getPropertyPath())
				.violation(v)
				.build()).collect(Collectors.toList()));

		List<Field> fields = Reflection.listFields(o.getClass()).stream()
				.filter(f -> f.getType() != Object.class && !f.getType().isPrimitive() && CLASSES_TO_SKIP.stream().noneMatch(classToSkip -> classToSkip.isAssignableFrom(f.getType())))
				.collect(Collectors.toList());

		for (Field field : fields) {

			String fieldName = field.getName();
			JsonProperty ann = field.getAnnotation(JsonProperty.class);

			if (ann != null)
				fieldName = ann.value();

			Object fieldValue = Reflection.getFieldValue(field, o);
			if (fieldValue != null)
				validateObject((path == null ? "" : path + ".") + fieldName, violations, fieldValue);
		}
	}

	private String violationsToMessage(List<ConstraintViolationWrapper> violations) {

		return violations.stream().collect(Collectors.groupingBy(cv -> cv.getViolation().getConstraintDescriptor().getAnnotation().getClass().getInterfaces()[0]))
				.entrySet().stream().map(entry -> {

					if (entry.getKey() == NotNull.class) {

						return "Missing payload fields: " + entry.getValue().stream()
								.map(ConstraintViolationWrapper::getPath)
								.collect(Collectors.joining(", "));
					}

					if (entry.getKey() == NotEmpty.class) {

						return "Missing or empty payload fields: " + entry.getValue().stream()
								.map(ConstraintViolationWrapper::getPath)
								.collect(Collectors.joining(", "));
					}

					if (entry.getKey() == Size.class) {

						return "Value length validation failed: " + entry.getValue().stream()
								.map(cv -> {

									Map<String, Object> attributes = cv.getViolation().getConstraintDescriptor().getAttributes();

									Integer min = (Integer) attributes.get("min");
									Integer max = (Integer) attributes.get("max");

									String suffix = null;

									if (min != 0 && max != Integer.MAX_VALUE)
										suffix = " must be between " + min + " and " + max + " characters";
									else if (min != 0)
										suffix = " must be at least " + min + " characters";
									else if (max != Integer.MAX_VALUE)
										suffix = " must be less than " + (max + 1) + " characters";

									if (suffix != null)
										return cv.getPath() + suffix;

									return null;
								})
								.filter(Objects::nonNull)
								.collect(Collectors.joining(", "));
					}

					if (entry.getKey() == Min.class) {

						return "Value validation failed: " + entry.getValue().stream()
								.map(cv -> {

									Map<String, Object> attributes = cv.getViolation().getConstraintDescriptor().getAttributes();

									Number min = (Number) attributes.get("value");

									String suffix = " must be greater than or equal to " + min.longValue();

									return cv.getPath() + suffix;
								})
								.collect(Collectors.joining(", "));
					}

					if (entry.getKey() == Max.class) {

						return "Value validation failed: " + entry.getValue().stream()
								.map(cv -> {

									Map<String, Object> attributes = cv.getViolation().getConstraintDescriptor().getAttributes();

									Number max = (Number) attributes.get("value");

									String suffix = " must be less than or equal to " + max.longValue();

									return cv.getPath() + suffix;
								})
								.collect(Collectors.joining(", "));
					}

					return "Unexpected validation class " + entry.getKey().getName();

				}).collect(Collectors.joining("; "));
	}

	@Builder
	@Getter
	private static class ConstraintViolationWrapper {

		private final String path;
		private final ConstraintViolation<Object> violation;
	}
}
