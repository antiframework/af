/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.reactive.web;

import com.af.commons.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.AbstractServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.SslInfo;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class ControllersLogging implements WebFilter {

	private final List<String> filters;

	public ControllersLogging(List<String> filters) {
		this.filters = filters;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {

		if (log.isDebugEnabled()) {

			ServerHttpRequest request = exchange.getRequest();
			String query = request.getQueryParams().entrySet().stream()
					.map(entry -> new AbstractMap.SimpleEntry<>(entry.getKey(), String.join(", ", entry.getValue())))
					.map(entry -> entry.getKey() + "=" + entry.getValue())
					.collect(Collectors.joining("&"));

			List<String> contentLengthHeaders = request.getHeaders().get("Content-Length");
			if (contentLengthHeaders != null && contentLengthHeaders.size() > 0 && Long.parseLong(contentLengthHeaders.get(0)) > 0) {

				ServerWebExchange.Builder exchangeMutator = exchange.mutate().request(new AbstractServerHttpRequest(request.getURI(), null, request.getHeaders()) {

					final List<ByteBuffer> buffers = new ArrayList<>();
					final Pointer<String> logMarker = new Pointer<>("aaa");

					@Override
					protected MultiValueMap<String, HttpCookie> initCookies() {
						return request.getCookies();
					}

					@Override
					protected SslInfo initSslInfo() {
						return request.getSslInfo();
					}

					@SuppressWarnings("unchecked")
					@Override
					public <T> T getNativeRequest() {
						return (T) request;
					}

					@Override
					public String getMethodValue() {
						return request.getMethodValue();
					}

					@Override
					public Flux<DataBuffer> getBody() {

						return request.getBody()
								.doOnNext(dataBuffer -> buffers.add(dataBuffer.asByteBuffer().duplicate()))
								.subscriberContext(ctx -> {

									logMarker.set(LogMarker.mark(ctx));
									return ctx;
								})
								.doOnComplete(() -> {

									ByteBuffer combined = ByteBuffer.allocate(buffers.stream().mapToInt(Buffer::remaining).sum());
									buffers.forEach(combined::put);

									String body = new String(combined.array());
									printRequest(logMarker.get(), request, query, body);
								});
					}
				});

				return chain.filter(exchangeMutator.build());
			}

			return LogMarker.<Void>mark(mark -> printRequest(mark, request, query, "{}"))
					.then(chain.filter(exchange));
		}

		return chain.filter(exchange);
	}

	private void printRequest(String mark, ServerHttpRequest request, String query, String body) {

		String requestPath = request.getPath().toString();
		if (filters.stream().noneMatch(requestPath::contains))
			log.debug(mark + ", " +  request.getMethod() + ": " + requestPath + (query.isEmpty() ? "" : "?") + query + " " + body.replaceAll("[ \n\t]+", " "));
	}
}
