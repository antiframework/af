/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.trx;

import com.af.jpa.AntiframeworkJpaException;

public interface AFTransaction {

	void commit() throws AntiframeworkJpaException;
	void rollback() throws AntiframeworkJpaException;
}
