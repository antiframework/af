/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.trx;

public class AFTrxMultiManager/* extends  _AFTrxManager*/ {

//	private Map<String, _AFTrxManager> allManagers;
//	private _AFMultiplexer<LinkedList<TrxContext>> trxMultiplexer;
//	private Supplier<LinkedList<TrxContext>> trxMultiplexerTypeSupplier;
//
//	public _AFTrxMultiManager(_AFMultiplexer<LinkedList<TrxContext>> trxMultiplexer) {
//
//		this.trxMultiplexer = trxMultiplexer;
//		allManagers = new ConcurrentHashMap<>();
//		trxMultiplexerTypeSupplier = LinkedList::new;
//	}
//
//	public void addTrxManager(String name, _AFTrxManager trxManager) {
//
//		allManagers.put(name, trxManager);
//	}
//
//	@Override
//	public _AFTransaction beginTransaction(String trxManagerName, boolean join) throws AntiframeworkJpaException {
//
//		if (trxManagerName == null)
//			throw new AntiframeworkJpaException("Unable to begin transaction. No Trx manager name is provided.");
//
//		LinkedList<TrxContext> trxContexts = trxMultiplexer.get(trxMultiplexerTypeSupplier);
//
//		while (trxContexts.descendingIterator().hasNext()) {
//
//			TrxContext prev = trxContexts.descendingIterator().next();
//
//			if (prev.getTrxName() != null && prev.getTrxName().equals(trxManagerName))
//				return prev.getTrxManager().beginTransaction(trxManagerName, join);
//		}
//
//		_AFTrxManager newTrxMgr = allManagers.get(trxManagerName);
//		if (newTrxMgr == null)
//			throw new AntiframeworkJpaException("Unable to begin transaction. No Trx manager for name (" + trxManagerName + ") is found");
//
//		trxContexts.addLast(new TrxContext(trxManagerName, newTrxMgr));
//
//		return newTrxMgr.beginTransaction(trxManagerName, join);
//	}
//
//	@Override
//	public Connection getConnection(String trxManagerName) throws AntiframeworkJpaException {
//		return null;
//	}
//
//	@AllArgsConstructor
//	@Getter
//	private static class TrxContext {
//
//		private String trxName;
//		private _AFTrxManager trxManager;
//	}
}
