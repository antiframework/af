/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.trx;

import com.af.commons.EnhancerUtils;
import com.af.commons.Reflection;
import com.af.jpa.AntiframeworkJpaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;

import javax.annotation.PostConstruct;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Slf4j
public class AFDataInstrumentator implements BeanPostProcessor {

	@Autowired
	private AFTrxManager trxManager;

	private Map<String, AFTrxInterceptor> interceptors;

	@PostConstruct
	public void init() {

		interceptors = new ConcurrentHashMap<>();
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

		try {

			List<Method> declaredMethods = Reflection.listMethods(bean.getClass());

			List<Method> transactionalMethods = declaredMethods.stream()
					.filter(m -> m.getAnnotation(AFTransactional.class) != null)
					.collect(Collectors.toList());

			if (transactionalMethods.isEmpty())
				return bean;

			Object enhanced = EnhancerUtils.enhance(bean.getClass(), (originalMethod, args, methodProxy) -> {

				AFTransactional trxAnn = originalMethod.getAnnotation(AFTransactional.class);
				if (trxAnn != null) {

					AFTrxInterceptor trxInterceptor;

					try {

						trxInterceptor = interceptors.computeIfAbsent(trxAnn.trxManager(), trxMgrName -> new AFTrxInterceptor(trxManager, trxAnn.trxManager()));
					} catch (Exception ex) {

						throw new AntiframeworkJpaException("Transaction manager not found for (" + trxAnn.trxManager() + ")", ex);
					}

					return trxInterceptor.handleInsideTransaction(trxAnn, () -> {

						try {

							return methodProxy.invoke(bean, args);
						} catch (Exception ex) {

							throw ex;
						} catch (Throwable th) {

							throw new AntiframeworkJpaException("Exception in transaction", th);
						}
					});
				} else
					return methodProxy.invoke(bean, args);
			});

			Method setAsBean = enhanced.getClass().getMethod("setAsBean", bean.getClass());
			if (setAsBean != null)
				setAsBean.invoke(enhanced, enhanced);

			return enhanced;
		} catch (Exception ex) {

			throw new BeanCreationException("Trx instrumentation failure.", ex);
		}
	}

//	private void populateFields(Class<?> clazz, Object bean, Object enhanced) throws IllegalAccessException {
//
//		if (clazz.equals(Object.class))
//			return;
//
//		Field[] declaredFields = clazz.getDeclaredFields();
//
//		for (Field field : declaredFields) {
//
//			int modifiers = field.getModifiers();
//			if (!Modifier.isFinal(modifiers) || !Modifier.isStatic(modifiers)) {
//
//				boolean accessible = field.isAccessible();
//				field.setAccessible(true);
//				field.set(enhanced, field.get(bean));
//				field.setAccessible(accessible);
//			}
//		}
//
//		populateFields(clazz.getSuperclass(), bean, enhanced);
//	}
}
