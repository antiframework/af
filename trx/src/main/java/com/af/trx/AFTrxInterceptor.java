/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.trx;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AFTrxInterceptor {

	private AFTrxManager trxManager;
	private String trxManagerName;

	public AFTrxInterceptor(AFTrxManager trxManager, String trxManagerName) {

		this.trxManager = trxManager;
		this.trxManagerName = trxManagerName;
	}

	<T> T handleInsideTransaction(AFTransactional ann, AFTrxManager.Routine<T> routine) throws Throwable {

		boolean join = ann.value() == AFTransactional.Isolation.JOIN;

		return trxManager.doWithTransaction(join, routine);
	}
}
