/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.trx;

import com.af.commons.EnhancerUtils;
import com.af.jpa.AntiframeworkJpaException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

@Slf4j
public abstract class AFTrxManager {

	private DataSource dataSource;

	protected AFTrxManager(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	protected abstract TrxContext trxContext();

	public <T> T doWithTransaction(boolean join, Routine<T> routine) throws Exception {

		AFTransaction trx = beginTransaction(join);

		try {

			T result = routine.go();
			trx.commit();

			return result;
		} catch (Exception ex) {

			trx.rollback();

			throw ex;
		}
	}

	public AFTransaction beginTransaction(boolean join) throws AntiframeworkJpaException {

		try {

			log.debug("Obtaining transaction, join: " + join);

			LinkedList<Connection> connections = trxContext().getConnections();

			if (join && !connections.isEmpty()) {

				log.debug("using existing connection");

				return new AFTransaction() {

					@Override
					public void commit() {

						//do nothing
						log.debug("Commit does nothing, using existing connection");
					}

					@Override
					public void rollback() {

						//do nothing
						log.debug("Rollback does nothing, using existing connection");
					}
				};
			}

			log.debug("creating new connection");

			Connection conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			connections.addLast(conn);

			return new AFTransaction() {

				@Override
				public void commit() throws AntiframeworkJpaException {

					try {

						log.debug("Committing connection");
						conn.commit();
					} catch (Exception ex) {

						throw new AntiframeworkJpaException("Commit failed", ex);
					} finally {

						closeAndRemove(conn, connections);
					}
				}

				@Override
				public void rollback() throws AntiframeworkJpaException {

					try {

						log.debug("Rolling back connection");
						conn.rollback();
					} catch (Exception ex) {

						throw new AntiframeworkJpaException("Commit failed", ex);
					} finally {

						closeAndRemove(conn, connections);
					}
				}
			};
		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Can't obtain transaction", ex);
		}
	}

	@SuppressWarnings("unused")
	public Connection getConnection() throws AntiframeworkJpaException {

		try {

			LinkedList<Connection> connections = trxContext().getConnections();

			if (connections.isEmpty()) {

				log.debug("Using stand alone connection");
				return dataSource.getConnection();
			}

			log.debug("Using connection from opened transaction");

			Connection last = connections.getLast();
			return EnhancerUtils.enhance(Connection.class, (originalMethod, args, methodProxy) -> {

				if (originalMethod.getName().equals("close")) {

					log.debug("Ignoring close connection, managed by transaction");
					return null;
				}
				else
					return methodProxy.invoke(last, args);
			});

		} catch (Exception ex) {

			throw new AntiframeworkJpaException("Unable to get connection", ex);
		}
	}

	private void closeAndRemove(Connection conn, LinkedList<Connection> connections) throws AntiframeworkJpaException {

		try {

			log.debug("Closing connection");
			conn.setAutoCommit(true);
			conn.close();
		} catch (SQLException ex) {

			throw new AntiframeworkJpaException("Exception closing connection", ex);
		}

		connections.removeLast();
	}

	public interface Routine<T> {

		T go() throws Exception;
	}

	@Getter
	public static class TrxContext {

		private LinkedList<Connection> connections;

		public TrxContext() {

			this.connections = new LinkedList<>();
		}
	}
}
