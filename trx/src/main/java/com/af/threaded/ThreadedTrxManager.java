/**
 * Michael Dubrovitsky<anti-framework.com>
 * https://bitbucket.org/antiframework/af
 */
package com.af.threaded;

import com.af.trx.AFTrxManager;

import javax.sql.DataSource;

public class ThreadedTrxManager extends AFTrxManager {

	private ThreadLocal<TrxContext> tl = new ThreadLocal<>();

	public ThreadedTrxManager(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected TrxContext trxContext() {

		TrxContext t = tl.get();

		if (t == null) {

			t = new TrxContext();
			tl.set(t);
		}

		return t;
	}
}
